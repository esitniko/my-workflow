#!/bin/bash

if [ -d workflow ]
then
  rm -r workflow
fi

yadage-run workflow workflow.yml -p list=list.txt -p prw=NTUP_PILEUP.13225134._000001.pool.root.1 -p mc=mc16a -p number=123456 -p tag=tag -d initdir=$PWD/inputdata

cat workflow/preparation/outputfile.txt
